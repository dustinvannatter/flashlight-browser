function Torch()
{
	this._isOn = false;
	var self = this;
	
	this.__defineGetter__("isOn", function() { return self._isOn; });	
}

Torch.prototype.turnOn = function()
{
	PhoneGap.exec("Torch.turnOn");
};

Torch.prototype.turnOff = function()
{
	PhoneGap.exec("Torch.turnOff");
};

Torch.install = function()
{
	if(!window.plugins) {
		window.plugins = {};
	}
	window.plugins.torch = new Torch();
};

PhoneGap.addConstructor(Torch.install);