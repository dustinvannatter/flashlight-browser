////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// SETTINGS ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

var ver = "0.97";
var uri = "https://su.uakron.edu";
var auth_uri = "https://www.uakron.edu/applications/suAuth.php";
var menu_offset = 310;
var bootstrapped = false;
var profile_complete = false;
var is_wizard_firstpage = false;
var loading_text = "Loading Data";
var searching_text = "Searching...";
var sharing_text = "Sharing...";
var generic_error = "An error occurred, try again!";
var specific_error = "An error occurred - "; // error message appended to end
var backGoal = "";
var data_menu;
var data_menu_total_new;
var calPlugin;
var gaPlugin;
var cb;
var notice_timeout;
var last_page;

var torch;
var url = "http://www.google.com/";
var click;

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BOOTSTRAP ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function init() {
	document.addEventListener("deviceready", deviceReady, true);
	$.event.special.swipe.horizontalDistanceThreshold = 75; // 80
	$.event.special.swipe.verticalDistanceThreshold = 35; // 50
	$.mobile.defaultHomeScroll = 0;
	delete init;
}

function destroy() {
	delete destroy;
}

function deviceReady() {
	document.addEventListener("offline", appOffline, true);
	document.addEventListener("online", appOnline, true);
	document.addEventListener("resume", appResume, true);

	cb = window.plugins.childBrowser;
	cb.onClose = childBrowserClosed;
	torch = window.plugins.torch;
	
    loadOnce();
//    $('img').retina();
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// APP CALLBACKS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function appOffline() {
}

function appOnline() {
}

function appResume() {
	if (localStorage["light_state"] != undefined) {
		if (localStorage["light_state"] == "ON") {
			$('.light_off').hide();
			$('.light_on').show();
			torch.turnOn();
		} else {
			$('.light_on').hide();
			$('.light_off').show();
			torch.turnOff();
		}
	}
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// PAGESHOW FUNCTIONS //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$('#main').live('pageshow', function(event, ui) {
});


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// TAP EVENTS //////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$('.hdr').live('tap', function(event, ui) { 
	$.mobile.changePage("#about", {transition: "fade"}); 
	return false;
});

$('#about').live('tap', function(event, ui) { 
	$.mobile.changePage("#main", {transition: "fade"}); 
	return false;
});


$('.light_btn').live('tap', function(event, ui) { 

	var state = (torch.isOn ? "ON" : "OFF");
	
	if (state == "ON") {
		// turn off
		torch.turnOff();
		$('.light_on').hide();
		$('.light_off').show();
		localStorage["light_state"] = "OFF";
		click.play({ playAudioWhenScreenIsLocked : false });
	} else {
		// turn on
		torch.turnOn();
		$('.light_off').hide();
		$('.light_on').show();
		localStorage["light_state"] = "ON";
		click.play({ playAudioWhenScreenIsLocked : false });
	}
	
});

$('.browse_btn').live('tap', function(event, ui) { 

	if ($("#urlurl").val().match(/^http:\/\//)) {
		var url = $('#urlurl').val();
	} else {
		var url = "http://" + $('#urlurl').val();
		$('#urlurl').val(url);
	}
	
	cb.showWebPage(url, { showLocationBar: true });
	localStorage["last_url"] = url;
	return false;
});


////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// GENERAL FUNCTIONS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

$('#urlurl').live('change keypress', function(event, ui) {
	if (event.keyCode == 13) {
		$('#urlurl').blur();
		$('.browse_btn').trigger('tap');
	}
});

function childBrowserClosed() {
}

function consolelog(log_msg) {
	console.log(log_msg); 
	$('#trace').append("> " + log_msg + "<br/>");	
}

function errorHandler(result) {
}

function handleLogin() {
}

function hideMenus() {
	$('.pullstring').trigger('tap');
}

function hideMobile() {
	$.mobile.loading('hide');
}

function loadOnce() {

	if (localStorage["light_state"] != undefined) {
		if (localStorage["light_state"] == "ON") {
			torch.turnOn();
			$('.light_off').hide();
			$('.light_on').show();
		} else {
			torch.turnOff();
			$('.light_on').hide();
			$('.light_off').show();
		}
	}

	if (localStorage["last_url"] != undefined) {
		$('#urlurl').val(localStorage["last_url"]);
	} else {
		$('#urlurl').val(url);
	}

	click = new Media('click.mp3', clickSuccess, clickError);
}

function clickSuccess() {
	
}

function clickError() {
	
}

function logout() {
	hideMobile();
	$('#logout').trigger("pageshow");
	$('.load_content').show();
	return false;
}

function notice(msg, delay) {
	clearTimeout(notice_timeout);
	$('#notice h5').text(msg);
	$('#notice').fadeIn();
	notice_timeout = setTimeout( function() { $("#notice").fadeOut(); }, delay );
}

function showCategory(urlObj, options) {
	if (profile_complete) { stageData(); }

	var category_id = urlObj.hash.replace( /.*category=/, "" );
	if (category_id == "first") {
		is_wizard_firstpage = true;
	}
	
	$('.wizard_stage').hide();
	$('#wizard_category_nav').html("");
	$('#wizard_category_content').html("");

	// get category information
	$.mobile.loading( 'show', {
		text: loading_text,
		textVisible: true,
		theme: 'e',
		html: ""
	});   
	
    $.ajax({
        type: "POST",
        url: uri + "/api/sub_categories.json",
        data: { user: localStorage["userid"], hash: localStorage["password_hash"], category_id: category_id },
        dataType: "json",
        timeout: 10000,
        success: function(data) {
			if (data.status.status_code == "200") {
			
				$('#wizard_cats .h3').html(data.category.Category.category_description);
				if (data.page.WizardPage.page_content) {
					$('#wizard_category_description').html(data.page.WizardPage.page_content);
				}

				cat_txt = "";				
				$.each(data.sub_categories, function(index) {
				
					cat_txt = cat_txt + "<div class='wizard_cat_row' id='wizard_cat_" + data.sub_categories[index].Category.id + "'><div class='wizard_cat_name' >" + data.sub_categories[index].Category.category_description + "</div>";
					cat_txt = cat_txt + "<div class='wizard_cat_buttons'>";
					cat_txt = cat_txt + "<fieldset data-role=\"controlgroup\" data-type=\"horizontal\" data-mini=\"true\">";

					if (data.sub_categories[index].UserCategory.status == "1") {
						cat_txt = cat_txt + "<input data-category_id=\"" + data.sub_categories[index].Category.id + "\" checked id=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "_1\" name=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "\" value=\"1\" type=\"radio\"><label for=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "_1\">Yes</label>";
					} else {
						cat_txt = cat_txt + "<input data-category_id=\"" + data.sub_categories[index].Category.id + "\" id=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "_1\" name=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "\" value=\"1\" type=\"radio\"><label for=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "_1\">Yes</label>";
					}
					
					if (data.sub_categories[index].UserCategory.status == "0") {
						cat_txt = cat_txt + "<input data-category_id=\"" + data.sub_categories[index].Category.id + "\" checked id=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "_0\" name=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "\" value=\"0\" type=\"radio\"><label for=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "_0\">No</label>";
					} else {
						cat_txt = cat_txt + "<input data-category_id=\"" + data.sub_categories[index].Category.id + "\" id=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "_0\" name=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "\" value=\"0\" type=\"radio\"><label for=\"wizard_cat_btn_" + data.sub_categories[index].Category.id + "_0\">No</label>";
					}
					cat_txt = cat_txt + "</fieldset></div></div>";
				});

				$('#wizard_category_content').html(cat_txt);

				// create navigation.. 
				nav = "";
				if ( (data.page.WizardPage.prev_page) && (!is_wizard_firstpage) ) {
					nav = nav + '<a href="' + data.page.WizardPage.prev_page + '" data-transition="fade" data-role="button">&lt;&lt; BACK</a>';
				}
				if (data.page.WizardPage.next_page) {
					nav = nav + '<a href="' + data.page.WizardPage.next_page + '" data-transition="fade" data-role="button">NEXT &gt;&gt;</a>';
				}
				$('#wizard_category_nav').html(nav);
				$('.wizard_stage').trigger('create');
				$('.wizard_stage').fadeIn();
				
			    hideMobile();
                gaPlugin.trackPage(successHandler, errorHandler, "/app/wizard/category/"+category_id);
            	$.mobile.changePage("#wizard_cats", {transition: "fade"}); 

			} else {
				hideMobile();
				notice(generic_error, 5000);			
			}
        },
        error: function(request, status, err) {
		    hideMobile();
			notice(specific_error + err, 5000);
        }
    });
}

function slideHome() {
}

function slideSubHome() {
}

function stageData(cat_id) {
}

function successHandler(result) {
}

function userLogin(user, hash) {
}

function userLogout() {
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// BUILDER FUNCTIONS ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////

function build_interruptor(d) {

	h = "";
	h = h + "<div class='interrupt'>";
	h = h + "<form action=''>";
	h = h + "<span class='interrupt_text'>Interested in this category?<br /><span class='interrupt_parent_category'>" + d.parent_category.Category.category_description + "</span> - \"" + d.category.Category.category_description + "\"</span>";
	h = h + "<div class='interrupt_btns' data-role='fieldcontain'  data-theme='c'><fieldset data-role='controlgroup' data-type='horizontal' data-mini='true'>";
	
	if (d.user_category.UserCategory.status == 1) {
		h = h + "<input class='interrupt_btn' data-action='1' data-parent_category_id='" + d.parent_category.Category.id + "' data-category_id='" + d.category.Category.id + "' id='interrupt_rdo_1' name='interruptor_btn' checked value='Yes' type='radio' /><label class='interrupt_btn' for='interrupt_rdo_1'>Yes</label>";
	} else {
		h = h + "<input class='interrupt_btn' data-action='1' data-parent_category_id='" + d.parent_category.Category.id + "' data-category_id='" + d.category.Category.id + "' id='interrupt_rdo_1' name='interruptor_btn' value='Yes' type='radio' /><label class='interrupt_btn' for='interrupt_rdo_1'>Yes</label>";
	}
	if (d.user_category.UserCategory.status == 0) {
		h = h + "<input class='interrupt_btn' data-action='0' data-parent_category_id='" + d.parent_category.Category.id + "' data-category_id='" + d.category.Category.id + "' id='interrupt_rdo_2' name='interruptor_btn' checked value='No' type='radio' /><label class='interrupt_btn' for='interrupt_rdo_2'>No</label>";
	} else {
		h = h + "<input class='interrupt_btn' data-action='0' data-parent_category_id='" + d.parent_category.Category.id + "' data-category_id='" + d.category.Category.id + "' id='interrupt_rdo_2' name='interruptor_btn' value='No' type='radio' /><label class='interrupt_btn' for='interrupt_rdo_2'>No</label>";
	}
	h = h + "</fieldset>";
	h = h + "</div></form></div>";
	return h;
}

function build_goal_row(d, t) {
	h = "";
	h = h + "<div data-id='" + d.UserGoal.id  + "' id='my_goal_" + d.UserGoal.id + "' class='goal_element_row'>";
	h = h + "<div class='goal_info'>";
	h = h + "<div class='goal_content'>" + d.UserGoal.goal_content + "</div>";
	h = h + "<div class='goal_category'>Related to: " + d.ParentCategory.category_description + " - \"" + d.Category.category_description + "\"</div>";
	if (t == "current") {
		h = h + "<div class='goal_created_content'>GOAL CREATED " + d.UserGoal.custom_created + "</div>";		
		h = h + "<input type='button' data-id='" + d.UserGoal.id  + "' id='complete_goal' data-role='button' data-inline='true' data-theme='f' data-mini='true' value='GOAL COMPLETED' />";	
	} else {
		h = h + "<div class='goal_completed_content'>GOAL COMPLETED " + d.UserGoal.custom_completed + "</div>";		
	}
	h = h + "</div>";

	h = h + "<div class='goal_actions'>";
	if (t == "current") {
		h = h + "<div data-id='" + d.UserGoal.id + "' class='goal_edit goal_action_" + d.UserGoal.id + " goal_element_row_icon uaicon edit'></div>";
	}
	h = h + "<div data-id='" + d.UserGoal.id + "' class='goal_delete goal_action_" + d.UserGoal.id + " goal_element_row_icon uaicon delete'></div>";
	h = h + "</div>";
	
	h = h + "</div>";
	return h;
}

function build_favorites_inspiration_row(d) {

	flags = "";
	removed = "";
	if (d.resource.status_id == 2) {
		flags = " removed ";
		removed = "<div class='removed_warning'>This item has expired or has been removed.</div>";
	}

	h = "";
	h = h + "<div data-id='" + d.resource.id  + "' id='my_favorite_" + d.resource.id + "' class='row_" + d.category.category_code + " fav_element_row " + flags + "'><div class='results_left'>";
	if (d.resource.is_university_resource == "1") {
		h = h + "<div class='fav_element_row_icon uatall uaicon_type " + d.resource_type.type_name + "'><span class=\"uahighlight\"></span></div>";
	} else {
		h = h + "<div class='fav_element_row_icon uaicon_type " + d.resource_type.type_name + "'><span class=\"uahighlight\"></span></div>";
	}
	h = h + "<div class='fav_element_row_content'>";
	if (d.resource_type.type_name == "inspirational_quote") {
		h = h + "\"" + d.quote.ResourceExtra.extra_value + "\"";
		h = h + "<span class=\"inspired_author\">";
		h = h + d.author.ResourceExtra.extra_value;
		h = h + "</span>";
	}
	if ( (d.resource_type.type_name == "inspirational_video") || (d.resource_type.type_name == "inspirational_audio") ) {
		if (!removed) {
			h = h + "<div class='cat_element_row_website_hdr'><a class=\"cat_element_a cb_call\" href=\"#\" data-href=\"" + d.resource.url + "\">" + d.resource.title + "</a></div>";
		} else {
			h = h + "<div class='cat_element_row_website_hdr'>" + d.resource.title + "</div>";
		}
		h = h + "<div class='cat_element_row_website_desc'>";
		h = h + d.resource.description;
		if (d.resource.is_mobile == "1") {
			h = h + " <span class='mobile_friendly'>(mobile friendly)</span>";
		}
		h = h + removed;
		h = h + "</div></a>";
	}	
	
	h = h + "</div></div><div class='results_right'>"; // results
	h = h + "<div class='uaicon_wrap'>";
	if (!removed) {
		h = h + "<div data-id='" + d.resource.id + "' data-type='i' data-src='myfavorites' class='fav_share fav_action_" + d.resource.id + " fav_element_row_icon uaicon share go_share'></div>";
	}
	h = h + "<div data-id='" + d.resource.id + "' data-type='i' class='fav_delete fav_action_" + d.resource.id + " fav_element_row_icon uaicon delete'></div>";
	h = h + "</div></div></div>";
	return h;
}

function build_favorites_resource_row(d) {

	flags = "";
	removed = "";
	if (d.resource.status_id == 2) {
		flags = " removed ";
		removed = "<div class='removed_warning'>This item has expired or has been removed.</div>";
	}

	h = "";
	h = h + "<div data-id='" + d.resource.id  + "' id='my_favorite_" + d.resource.id + "' class='row_" + d.category.category_code + " fav_element_row " + flags + "'><div class='results_left'>";
	if (d.resource.is_university_resource == "1") {
		h = h + "<div class='fav_element_row_icon uatall uaicon_type " + d.resource_type.type_name + "'><span class=\"uahighlight\"></span></div>";
	} else {
		h = h + "<div class='fav_element_row_icon uaicon_type " + d.resource_type.type_name + "'><span class=\"uahighlight\"></span></div>";
	}
	h = h + "<div class='fav_element_row_content'>";
	if (d.resource_type.type_name == "website") {
		if (!removed) {
			h = h + "<div class='cat_element_row_website_hdr'><a class=\"cat_element_a cb_call\" href=\"#\" data-href=\"" + d.resource.url + "\">" + d.resource.title + "</a></div>";
		} else {
			h = h + "<div class='cat_element_row_website_hdr'>" + d.resource.title + "</div>";
		}
		h = h + "<div class='cat_element_row_website_desc'>";
		h = h + d.resource.description;
		if (d.resource.is_mobile == "1") {
			h = h + " <span class='mobile_friendly'>(mobile friendly)</span>";
		}
		h = h + removed;
		h = h + "</div>";
	} else {
		h = h + "<div class='cat_element_row_website_hdr'><a class=\"cat_element_a cb_call\" href=\"#\" data-href=\"" + d.resource.url + "\">" + d.resource.title + "</a></div>";
		h = h + "<div class='cat_element_row_website_desc'>";
		h = h + d.resource.description;
		if (d.resource.is_mobile == "1") {
			h = h + " <span class='mobile_friendly'>(mobile friendly)</span>";
		}
		h = h + removed;
		h = h + "</div>";
	}
	h = h + "</div></div><div class='results_right'>"; // results
	h = h + "<div class='uaicon_wrap'>";
	if (!removed) {
		h = h + "<div data-id='" + d.resource.id + "' data-type='r' data-src='myfavorites' class='fav_share fav_action_" + d.resource.id + " fav_element_row_icon uaicon share go_share'></div>";
	}
	h = h + "<div data-id='" + d.resource.id + "' data-type='r' class='fav_delete fav_action_" + d.resource.id + " fav_element_row_icon uaicon delete'></div>";
	h = h + "</div></div></div>";	
	return h;
}

/** category resource builders **/

function build_inspiration_row(d, type) {

	flags = "";
	prepend = "";
	cats = "";	
	
	if (d[0].really_new == "1") {
		flags += " new_item ";
		prepend = "<span class='flag_new'><i>NEW</i></span> ";
	}
	if (d[0].really_hot == "1") {
		flags += " hot_topic ";
		prepend = "<span class='flag_hot'><i>HOT</i></span> ";
	}

	if (type == "new") {
		$.each(d.categories, function(index) {
			cats += "filter_cat_" + d.categories[index].Category.id + " ";
		});	
	}

	h = "";
	h = h + "<div data-id='" + d.r.id  + "' class='cat_element_row " + flags + cats + "'><div class='results_left'>";
	if (d.r.is_university_resource == "1") {
		h = h + "<div class='cat_element_row_icon uatall uaicon_type " + d.rt.type_name + "'><span class=\"uahighlight\"></span></div>";
	} else {
		h = h + "<div class='cat_element_row_icon uaicon_type " + d.rt.type_name + "'><span class=\"uahighlight\"></span></div>";
	}
	h = h + "<div class='cat_element_row_content'>";
	if (d.rt.type_name == "inspirational_quote") {
		h = h + prepend + "\"" + d[0].quote + "\"";
		h = h + "<span class=\"inspired_author\">";
		h = h + d[0].author;
		h = h + "</span>";
	}
	if ( (d.rt.type_name == "inspirational_video") || (d.rt.type_name == "inspirational_audio") ) {
		h = h + "<div class='cat_element_row_website_hdr'><a class=\"cat_element_a cb_call\" href=\"#\" data-href=\"" + d.r.url + "\">" + prepend + d.r.title + "</a></div>";
		h = h + "<div class='cat_element_row_website_desc'>";
		h = h + d.r.description;
		if (d.r.is_mobile == "1") {
			h = h + " <span class='mobile_friendly'>(mobile friendly)</span>";
		}
		h = h + "</div></a>";
	}
	
	h = h + "</div></div><div class='results_right'>"; // results
	h = h + "<div class='uaicon_wrap'>";
	if (d.uf.id) {
		h = h + "<div data-id='" + d.r.id + "' data-type='i' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon fave fave_on'></div>";
	} else {
		h = h + "<div data-id='" + d.r.id + "' data-type='i' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon fave'></div>";
	}
	h = h + "<div data-id='" + d.r.id + "' data-type='i' data-src='category' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon share go_share'></div>";
	h = h + "<div style='display:none;' class='cat_read_" + d.r.id + " cat_element_markread'><a href='javascript:;' data-role='button' data-inline='true' data-mini='true' data-theme='b'>Read</a></div>";
	h = h + "</div></div></div>";
	
	return h;
}

function build_resource_row(d, type) {

	flags = "";
	prepend = "";
	cats = "";
	
	if (d[0].really_new == "1") {
		flags += " new_item ";
		prepend = "<span class='flag_new'><i>NEW</i></span> ";
	}	
	if (d[0].really_hot == "1") {
		flags += " hot_topic ";
		prepend = "<span class='flag_hot'><i>HOT</i></span> ";
	}
	
	if (type == "new") {
		$.each(d.categories, function(index) {
			cats += "filter_cat_" + d.categories[index].Category.id + " ";
		});	
	}

	h = "";
	h = h + "<div data-id='" + d.r.id  + "' class='cat_element_row " + flags + cats + "'><div class='results_left'>";
	if (d.r.is_university_resource == "1") {
		h = h + "<div class='cat_element_row_icon uatall uaicon_type " + d.rt.type_name + "'><span class=\"uahighlight\"></span></div>";
	} else {
		h = h + "<div class='cat_element_row_icon uaicon_type " + d.rt.type_name + "'><span class=\"uahighlight\"></span></div>";
	}
	
	h = h + "<div class='cat_element_row_content'>";
	if (d.rt.type_name == "website") {
		h = h + "<div class='cat_element_row_website_hdr'><a class=\"cat_element_a cb_call\" href=\"#\" data-href=\"" + d.r.url + "\">" + prepend + d.r.title + "</a></div>";
		h = h + "<div class='cat_element_row_website_desc'>";
		h = h + d.r.description;
		if (d.r.is_mobile == "1") {
			h = h + " <span class='mobile_friendly'>(mobile friendly)</span>";
		}
		h = h + "</div></a>";
	} else {
		h = h + "<div class='cat_element_row_website_hdr'><a class=\"cat_element_a cb_call\" href=\"#\" data-href=\"" + d.r.url + "\">" + prepend + d.r.title + "</a></div>";
		h = h + "<div class='cat_element_row_website_desc'>";
		h = h + d.r.description;
		if (d.r.is_mobile == "1") {
			h = h + " <span class='mobile_friendly'>(mobile friendly)</span>";
		}
		h = h + "</div></a>";
	}

	h = h + "</div></div><div class='results_right'>"; // results
	h = h + "<div class='uaicon_wrap'>";
	
	if (d.uf.id) {
		h = h + "<div data-id='" + d.r.id + "' data-type='r' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon fave fave_on'></div>";
	} else {
		h = h + "<div data-id='" + d.r.id + "' data-type='r' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon fave'></div>";
	}
	h = h + "<div data-id='" + d.r.id + "' data-type='r' data-src='category' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon share go_share'></div>";
	h = h + "<div style='display:none;' class='cat_read_" + d.r.id + " cat_element_markread'><a href='javascript:;' data-role='button' data-inline='true' data-mini='true' data-theme='b'>Read</a></div>";
	h = h + "</div></div></div>";	
	return h;
}


function build_myevents_row(d) {

	flags = "";
	removed = "";
	cats = "";

	if (d.event.expired == 1) {
		flags = " removed ";
		removed = "<div class='removed_warning'>This item has expired or has been removed.</div>";
	}
	
	$.each(d.categories, function(index) {
		cats += "myevent_cat_" + d.categories[index].category_id + " ";
	});	
	
	h = "<div class='myevents_row_overall " + cats + flags + "'>";
	h = h + "<div data-id='" + d.event.id  + "' id='myevent_" + d.event.id + "' class='myevents_element_row'><div class='results_left'>";
	h = h + "<div data-id='" + d.event.id + "' data-type='myevents_' class='element_event_cal toggle_more'><div class='calendarrow'><div class='themonth m" + d.special.month + "'><span></span></div><div class='theday d" + d.special.day + "'></div></div></div>";
	h = h + "<div class='element_event_info'>";					
	h = h + "<div data-id='" + d.event.id + "' data-type='myevents_' class='element_event_title toggle_more'>";					
	h = h + d.event.event_name;	
	h = h + "</div>";
	h = h + "<div class='element_event_desc'>";					
	if (d.event.title) {
		h = h + d.event.title + "<br/>";	
	}
	if (d.event.event_type) {
		h = h + "<div class='element_event_type'>(";
		h = h + d.event.event_type;		
		h = h + ")</div>";
	}
	h = h + "<div class='element_event_locations'>";
	first = 0;
	$.each(d.spaces, function(ix) {
		if (first == 0) {
			h = h + "<div class='element_event_location'>" + d.spaces[ix].formal_name + "</div>";
			first = 1;
		}
	});
	h = h + "</div>";
	
	h = h + "<div class='element_event_datetime'>";
	if (d.special.formatted_start_day == d.special.formatted_end_day) {
		h = h + d.special.formatted_start + " to " + d.special.formatted_end_time;	
	} else {
		h = h + d.special.formatted_start + " to " + d.special.formatted_end;	
	}
	h = h + "</div>";
	h = h + removed;
	h = h + "</div>";
	h = h + "</div>";
	h = h + "</div><div class='results_right'>"; // results

	h = h + "<div class='uaicon_wrap'>";
	if (!removed) {
		h = h + "<div data-id='" + d.event.id + "' data-type='e' data-src='myevents' class='cat_action_" + d.event.id + " myevents_element_row_icon uaicon share go_share'></div>";
	}
	if ( (device.platform == "iPhone") || (device.platform == "iPhone Simulator") ) {
		if (!removed) {
			h = h + "<div data-id='" + d.event.id + "' class='cat_action_" + d.event.id + " myevents_element_row_icon uaicon add_ical'></div>";
		}
	}
	h = h + "<div data-id='" + d.event.id + "' class='cat_action_" + d.event.id + " myevents_element_row_icon uaicon myevents_delete delete'></div>";
	h = h + "</div></div>";
	h = h + "</div>"; // /results

	if (!removed) {
		h = h + "<div id='myevents_event_details_" + d.event.id + "' data-id='" + d.event.id + "' class='element_event_details' style='display:none;'>";
		if (d.event.description) {
			h = h + "<div class='element_event_details_description_lbl'>Description:</div>";
			h = h + "<div class='element_event_details_description'>";
			h = h + d.event.description;
			h = h + "</div>";	
		}
		if (d.event.event_locator) {
			h = h + "<div class='element_event_details_lineitem'><b>Reference:</b> " + d.event.event_locator + "</div>";	
		}
		if (d.event.organization_title) {
			h = h + "<div class='element_event_details_lineitem'><b>Organization:</b> " + d.event.organization_title + "</div>";	
		} else {
			if (d.event.organization_name) {
				h = h + "<div class='element_event_details_lineitem'><b>Organization:</b> " + d.event.organization_name + "</div>";	
			}
		}
		if (d.event.attendee_count) {
			h = h + "<div class='element_event_details_lineitem'><b>Head Count:</b> " + d.event.attendee_count + "</div>";	
		}
		h = h + "</div>";	
		h = h + "<div data-id='" + d.event.id + "' id='myevents_event_toggle_" + d.event.id + "' data-type='myevents_' class='element_event_details_toggle'>+ MORE</div>";
	}
	
	h = h + "</div></div>";	
	return h;
}


function build_event_row(d, type) {

	flags = "";
	prepend = "";
	cats = "";
	
	if (d[0].really_new == "1") {
		flags += " new_item ";
		prepend = "<span class='flag_new'><i>NEW:</i></span> ";
	}
	
	if (type == "new") {
		$.each(d.categories, function(index) {
			cats += "filter_cat_" + d.categories[index].Category.id + " ";
		});	
	}

	h = "";
	h = h + "<div data-id='" + d.e.id  + "' class='cat_element_row " + flags + cats + "'><div class='results_left'>";
	h = h + "<div data-id='" + d.e.id + "' data-type='search_' class='element_event_cal toggle_more'><div class='calendarrow'><div class='themonth m" + d[0].month + "'><span></span></div><div class='theday d" + d[0].day + "'></div></div></div>";
	h = h + "<div class='element_event_info'>";					
	h = h + "<div data-id='" + d.e.id + "' data-type='search_' class='element_event_title toggle_more'>";					
	h = h + prepend + d.e.event_name;	
	h = h + "</div>";
	h = h + "<div class='element_event_desc'>";					
	if (d.e.title) {
		h = h + d.e.title + "<br/>";	
	}
	if (d.e.event_type) {
		h = h + "<div class='element_event_type'>(";
		h = h + d.e.event_type;		
		h = h + ")</div>";
	}
	h = h + "<div class='element_event_locations'>";
	first = 0;
	$.each(d.es, function(ix) {
		if (first == 0) {
			h = h + "<div class='element_event_location'>" + d.es[ix].event_spaces.formal_name + "</div>";
			first = 1;
		}
	});
	h = h + "</div>";
	
	h = h + "<div class='element_event_datetime'>";
	if (d[0].formatted_start_day == d[0].formatted_end_day) {
		h = h + d[0].formatted_start + " to " + d[0].formatted_end_time;	
	} else {
		h = h + d[0].formatted_start + " to " + d[0].formatted_end;	
	}
	h = h + "</div>";
	h = h + "</div>";
	h = h + "</div>";
	h = h + "</div><div class='results_right'>"; // results
	h = h + "<div class='uaicon_wrap'>";
	if (d.ue.id) {
		h = h + "<div data-id='" + d.e.id + "' data-type='e' class='cat_action_" + d.e.id + " cat_element_row_icon uaicon event_toggle event event_on'></div>";
	} else {
		h = h + "<div data-id='" + d.e.id + "' data-type='e' class='cat_action_" + d.e.id + " cat_element_row_icon uaicon event_toggle event'></div>";
	}
	if ( (device.platform == "iPhone") || (device.platform == "iPhone Simulator") ) {
		h = h + "<div data-id='" + d.e.id + "' class='cat_action_" + d.e.id + " cat_element_row_icon uaicon add_ical'></div>";
	}
	h = h + "<div data-id='" + d.e.id + "' data-type='e' data-src='category' class='cat_action_" + d.e.id + " cat_element_row_icon uaicon share go_share'></div>";

	h = h + "</div></div>";
	h = h + "</div>"; // /results

	h = h + "<div id='search_event_details_" + d.e.id + "' data-id='" + d.e.id + "' class='element_event_details' style='display:none;'>";
	if (d.e.description) {
		h = h + "<div class='element_event_details_description_lbl'>Description:</div>";
		h = h + "<div class='element_event_details_description'>";
		h = h + d.e.description;
		h = h + "</div>";	
	}
	if (d.e.event_locator) {
		h = h + "<div class='element_event_details_lineitem'><b>Reference:</b> " + d.e.event_locator + "</div>";	
	}
	if (d.e.organization_title) {
		h = h + "<div class='element_event_details_lineitem'><b>Organization:</b> " + d.e.organization_title + "</div>";	
	} else {
		if (d.e.organization_name) {
			h = h + "<div class='element_event_details_lineitem'><b>Organization:</b> " + d.e.organization_name + "</div>";	
		}
	}
	if (d.e.attendee_count) {
		h = h + "<div class='element_event_details_lineitem'><b>Head Count:</b> " + d.e.attendee_count + "</div>";	
	}
	h = h + "</div>";	
	h = h + "<div data-id='" + d.e.id + "' id='search_event_toggle_" + d.e.id + "' data-type='search_' class='element_event_details_toggle'>+ MORE</div>";

	h = h + "</div>";	
	return h;
}


/** search builders **/

function build_search_inspiration_row(d) {

	flags = "";
	prepend = "";
	
	if (d[0].really_new == "1") {
		flags += " new_item ";
		prepend = "<span class='flag_new'><i>NEW</i></span> ";
	}
	if (d[0].really_hot == "1") {
		flags += " hot_topic ";
		prepend = "<span class='flag_hot'><i>HOT</i></span> ";
	}
	
	h = "";
	h = h + "<div data-id='" + d.r.id  + "' id='search_" + d.r.id + "' class='search_element_row " + flags + "'><div class='results_left'>";
	if (d.r.is_university_resource == "1") {
		h = h + "<div class='search_element_row_icon uatall uaicon_type " + d.rt.type_name + "'><span class=\"uahighlight\"></span></div>";
	} else {
		h = h + "<div class='search_element_row_icon uaicon_type " + d.rt.type_name + "'><span class=\"uahighlight\"></span></div>";
	}
	h = h + "<div class='search_element_row_content'>";
	if (d.rt.type_name == "inspirational_quote") {
		h = h + prepend + "\"" + d[0].quote + "\"";
		h = h + "<span class=\"inspired_author\">";
		h = h + d[0].author;
		h = h + "</span>";
	}
	h = h + "</div></div><div class='results_right'>"; // results
	h = h + "<div class='uaicon_wrap'>";
	if (d.uf.id) {
		h = h + "<div data-id='" + d.r.id + "' data-type='i' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon fave fave_on'></div>";
	} else {
		h = h + "<div data-id='" + d.r.id + "' data-type='i' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon fave'></div>";
	}
	h = h + "<div data-id='" + d.r.id + "' data-type='i' data-src='search' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon share go_share'></div>";
	h = h + "</div></div></div>";
	return h;
}

function build_search_resource_row(d) {

	flags = "";
	prepend = "";

	if (d[0].really_new == "1") {
		flags += " new_item ";
		prepend = "<span class='flag_new'><i>NEW</i></span> ";
	}
	if (d[0].really_hot == "1") {
		flags += " hot_topic ";
		prepend = "<span class='flag_hot'><i>HOT</i></span> ";
	}
	
	h = "";
	h = h + "<div data-id='" + d.r.id  + "' id='search_" + d.r.id + "' class='search_element_row " + flags + "'><div class='results_left'>";
	if (d.r.is_university_resource == "1") {
		h = h + "<div class='search_element_row_icon uatall uaicon_type " + d.rt.type_name + "'><span class=\"uahighlight\"></span></div>";
	} else {
		h = h + "<div class='search_element_row_icon uaicon_type " + d.rt.type_name + "'><span class=\"uahighlight\"></span></div>";
	}
	h = h + "<div class='search_element_row_content'>";
	if (d.rt.type_name == "website") {
		h = h + "<div class='cat_element_row_website_hdr'><a class=\"cat_element_a cb_call\" href=\"#\" data-href=\"" + d.r.url + "\">" + prepend + d.r.title + "</a></div>";
		h = h + "<div class='cat_element_row_website_desc'>";
		h = h + d.r.description;
		if (d.r.is_mobile == "1") {
			h = h + " <span class='mobile_friendly'>(mobile friendly)</span>";
		}
		h = h + "</div></a>";
	} else {
		h = h + "<div class='cat_element_row_website_hdr'><a class=\"cat_element_a cb_call\" href=\"#\" data-href=\"" + d.r.url + "\">" + prepend + d.r.title + "</a></div>";
		h = h + "<div class='cat_element_row_website_desc'>";
		h = h + d.r.description;
		if (d.r.is_mobile == "1") {
			h = h + " <span class='mobile_friendly'>(mobile friendly)</span>";
		}
		h = h + "</div></a>";
	}
	h = h + "</div></div><div class='results_right'>"; // results
	h = h + "<div class='uaicon_wrap'>";
	if (d.uf.id) {
		h = h + "<div data-id='" + d.r.id + "' data-type='r' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon fave fave_on'></div>";
	} else {
		h = h + "<div data-id='" + d.r.id + "' data-type='r' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon fave'></div>";
	}
	h = h + "<div data-id='" + d.r.id + "' data-type='r' data-src='search' class='cat_action_" + d.r.id + " cat_element_row_icon uaicon share go_share'></div>";
	h = h + "</div></div></div>";	
	return h;
}

function build_search_event_row(d) {

	flags = "";
	prepend = "";
	if (d[0].really_new == "1") {
		flags += " new_item ";
		prepend = "<span class='flag_new'><i>NEW:</i></span> ";
	}
	
	h = "";
	h = h + "<div data-id='" + d.e.id  + "' id='search_" + d.e.id + "' class='search_element_row " + flags + "'><div class='results_left'>";
	h = h + "<div data-id='" + d.e.id + "' data-type='search_' class='element_event_cal toggle_more'><div class='calendarrow'><div class='themonth m" + d[0].month + "'><span></span></div><div class='theday d" + d[0].day + "'></div></div></div>";
	h = h + "<div class='element_event_info'>";					
	h = h + "<div data-id='" + d.e.id + "' data-type='search_' class='element_event_title toggle_more'>";					
	h = h + prepend + d.e.event_name;	
	h = h + "</div>";
	h = h + "<div class='element_event_desc'>";					
	if (d.e.title) {
		h = h + d.e.title + "<br/>";	
	}

	if (d.e.event_type) {
		h = h + "<div class='element_event_type'>(";
		h = h + d.e.event_type;		
		h = h + ")</div>";
	}
	h = h + "<div class='element_event_locations'>";
	first = 0;
	$.each(d.es, function(ix) {
		if (first == 0) {
			h = h + "<div class='element_event_location'>" + d.es[ix].event_spaces.formal_name + "</div>";
			first = 1;
		}
	});
	h = h + "</div>";
					
	h = h + "<div class='element_event_datetime'>";
	if (d[0].formatted_start_day == d[0].formatted_end_day) {
		h = h + d[0].formatted_start + " to " + d[0].formatted_end_time;	
	} else {
		h = h + d[0].formatted_start + " to " + d[0].formatted_end;	
	}
	h = h + "</div>";
	h = h + "</div>";
	h = h + "</div>";
	h = h + "</div><div class='results_right'>"; // results
	h = h + "<div class='uaicon_wrap'>";
	if (d.ue.id) {
		h = h + "<div data-id='" + d.e.id + "' data-type='e' class='cat_action_" + d.e.id + " cat_element_row_icon uaicon event_toggle event event_on'></div>";
	} else {
		h = h + "<div data-id='" + d.e.id + "' data-type='e' class='cat_action_" + d.e.id + " cat_element_row_icon uaicon event_toggle event'></div>";
	}
	if ( (device.platform == "iPhone") || (device.platform == "iPhone Simulator") ) {
		h = h + "<div data-id='" + d.e.id + "' class='cat_action_" + d.e.id + " cat_element_row_icon uaicon add_ical'></div>";
	}
	h = h + "<div data-id='" + d.e.id + "' data-type='e' data-src='search' class='cat_action_" + d.e.id + " cat_element_row_icon uaicon share go_share'></div>";
	h = h + "</div></div>";
	h = h + "</div>"; // /results

	h = h + "<div id='search_event_details_" + d.e.id + "' data-id='" + d.e.id + "' class='element_event_details' style='display:none;'>";
	if (d.e.description) {
		h = h + "<div class='element_event_details_description_lbl'>Description:</div>";
		h = h + "<div class='element_event_details_description'>";
		h = h + d.e.description;
		h = h + "</div>";	
	}
	if (d.e.event_locator) {
		h = h + "<div class='element_event_details_lineitem'><b>Reference:</b> " + d.e.event_locator + "</div>";	
	}
	if (d.e.organization_title) {
		h = h + "<div class='element_event_details_lineitem'><b>Organization:</b> " + d.e.organization_title + "</div>";	
	} else {
		if (d.e.organization_name) {
			h = h + "<div class='element_event_details_lineitem'><b>Organization:</b> " + d.e.organization_name + "</div>";	
		}
	}
	if (d.e.attendee_count) {
		h = h + "<div class='element_event_details_lineitem'><b>Head Count:</b> " + d.e.attendee_count + "</div>";	
	}
	h = h + "</div>";	
	h = h + "<div data-id='" + d.e.id + "' id='search_event_toggle_" + d.e.id + "' data-type='search_' class='element_event_details_toggle'>+ MORE</div>";

	h = h + "</div>";	
	return h;
}
