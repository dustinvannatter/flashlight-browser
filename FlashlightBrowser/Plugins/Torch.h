#import <Foundation/Foundation.h>
#import <Cordova/CDV.h>

@interface Torch : CDVPlugin  {
	 AVCaptureSession* session;
}

@property (nonatomic, retain) AVCaptureSession* session;

- (void) turnOn:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options;
- (void) turnOff:(NSMutableArray*)arguments withDict:(NSMutableDictionary*)options;

@end